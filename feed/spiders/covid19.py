# -*- coding: utf-8 -*-
import scrapy
from feed.items import Covid19Item


class Covid19Spider(scrapy.Spider):
    name = 'covid19'
    allowed_domains = ['worldometers.info']
    start_urls = ['https://www.worldometers.info/coronavirus']
    
    def parse_detail(self,response,country_name=None,total_cases=None,new_cases=None,total_deaths=None,new_deaths=None):
        #Get the detail of the country. In this case only apply to USA for the momment.
        table_country = response.css('table.table.table-responsive')
        if table_country is not None:
            #If we have detail of the parent, Get it here
            covidItem = Covid19Item()
            covidItem['country_name'] = country_name
            covidItem['total_cases'] = total_cases
            covidItem['new_cases'] = new_cases
            covidItem['total_deaths'] = total_deaths
            covidItem['new_deaths'] = new_deaths
            arr = []
            #covidCityItem = None
            for city in table_country.css('tbody tr'):
                covidCityItem = Covid19Item()
                covidCityItem['country_name'] = city.css('td:nth-child(1)::text').get(default='').strip()
                covidCityItem['total_cases'] = city.css('td:nth-child(2)::text').get(default='').strip()
                covidCityItem['new_cases'] = city.css('td:nth-child(3)::text').get(default='').strip()
                covidCityItem['total_deaths'] = city.css('td:nth-child(4)::text').get(default='').strip()
                covidCityItem['new_deaths'] = city.css('td:nth-child(5)::text').get(default='').strip()
                arr.append(covidCityItem)
                
            #if covidCityItem is not None:
            covidItem['detail'] = arr
            yield (covidItem)

    def parse(self, response):
        for countries in response.css('#main_table_countries_today tbody tr'):
            covidItem = Covid19Item()
            covidItem['country_name'] = countries.css('td:nth-child(1) ::text').get(default='').strip()
            link_country = countries.css('td:nth-child(1) a::attr(href)').get()
            
            covidItem['total_cases'] = countries.css('td:nth-child(2)::text').get(default='').strip()
            covidItem['new_cases'] = countries.css('td:nth-child(3)::text').get(default='').strip()
            covidItem['total_deaths'] = countries.css('td:nth-child(4)::text').get(default='').strip()
            covidItem['new_deaths'] = countries.css('td:nth-child(5)::text').get(default='').strip()
            
            #Only USA have detail by States, so only get the details of this country
            #if link_country is not None:
            if covidItem['country_name'] == 'USA':
                #link_country = response.urljoin(link_country)
                #yield scrapy.Request(link_country,callback=self.parse)
                yield response.follow(link_country,callback=self.parse_detail,cb_kwargs=dict(covidItem))
            else:
                yield(covidItem)
        

            
        
        
        
            
        
