# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html
from scrapy.exceptions import DropItem
from scrapy.utils.serialize import ScrapyJSONEncoder

import sqlite3, json, sys, os, time
from sqlite3 import Error

class AddSourceName(object):
    def process_item(self, item, spider):
        sources ={
            'tmz':'TMZ',
            'covid19':'COVID19',
            }
        item['source'] = sources.get(spider.name,'Undefined')
        return item

class SaveToDB(object):
    
    def __init__(self,sqllite):
        #self.sqllite = os.path.join(sys.path[0] , sqllite)
        self.sqllite = sqllite

    @classmethod
    def from_crawler(cls,crawler):
        return cls(
            sqllite = crawler.settings.get('SQLLITE_DB')
            )

    def open_spider(self, spider):
        #spider.logger.info('Information:' + self.sqllite)
        self.conn = sqlite3.connect(self.sqllite)
        self.cursor = self.conn.cursor()
        #create table tmz
        self.cursor.execute(self.__create_table())
        self.cursor.execute(self.__create_index())
        #create table covid19
        self.cursor.execute(self.__create_table_covid19())
        #creates covid19 table indexes
        self.cursor.execute(self.__create_index_covid19())
        self.cursor.execute(self.__create_index_covid19_date())
        
        
        
    def close_spider(self, spider):
        if self.conn:
            self.conn.commit()
            self.conn.close()
        
    def process_item(self, item, spider):
        if spider.name == 'tmz':
            self.cursor.execute('insert or ignore into info(hash,title,article,published,raw) values(?,?,?,?,?)', [item['hash'],item['header'],item['article'],0,ScrapyJSONEncoder().encode(item)])
        elif spider.name == 'covid19':
            self.cursor.execute('insert or ignore into covid19(country,raw,date) values(?,?,?)',[item['country_name'],ScrapyJSONEncoder().encode(item),time.time()])
        
        return item
    def __create_table(self):
        sql_table = """
    CREATE TABLE IF NOT EXISTS info(
    id integer PRIMARY KEY,
    hash text NOT NULL,
    title text,
    article text,
    raw text,
    published integer );
    """
        return sql_table
        
    def __create_table_covid19(self):
        sql_table = """
    CREATE TABLE IF NOT EXISTS covid19(
    id integer PRIMARY KEY,
    country text,
    raw text,
    date integer);
    """
        return sql_table
    
    def __create_index(self):
        sql_index = """
    CREATE UNIQUE INDEX IF NOT EXISTS hash_index on info (hash);
    """
        return sql_index
        
    def __create_index_covid19(self):
        sql_index = """
    CREATE UNIQUE INDEX IF NOT EXISTS name_index on covid19 (country);
    """
        return sql_index
    def __create_index_covid19_date(self):
        sql_index = """
    CREATE UNIQUE INDEX IF NOT EXISTS date_index on covid19 (date);
    """
        return sql_index

    
