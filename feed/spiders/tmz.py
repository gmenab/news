# -*- coding: utf-8 -*-
import scrapy
import hashlib


class TmzSpider(scrapy.Spider):
    name = 'tmz'
    allowed_domains = ['tmz.com']
    start_urls = ['http://tmz.com/']

    def parse(self, response):
        for articles in response.css('article.article'):
            title = ''
            title += articles.css('header h2 span.h1::text').get(default='') + ' '
            title += articles.css('header h2 span.h2::text').get(default='') + ' '
            title += articles.css('header h2 span.h3::text').get(default='')
            title = title.strip()
            
            yield{
                'header': title,
                'hash': hashlib.md5(title.encode('utf-8')).hexdigest(), 
                'article': ' '.join(map(str,articles.css('div.article__blocks section.canvas-text-block p ::text').getall())),
                'url': articles.css('header.article__header a::attr(href)').get(),
                'image_urls': articles.css('div.article__blocks img::attr(src)').getall(),
            }
